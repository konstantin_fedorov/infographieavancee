$input v_normal, v_tangent, v_wpos, v_shadowcoord, v_texcoord0

#include "common.sh"

#define LTC_NUM_POINTS 4
#define LTC_TEXTURED   1
#define shadowMapBias u_params1.z
#include "ltc.sh"
// -------------------------------------------------------------------------------------------------
void main()
{
	vec3 Lo_i;
    
	if(u_displayMode == 0 || u_displayMode == 1) {
		ShadingContext s = InitShadingContext(v_normal, v_tangent, v_wpos, u_viewPosition.xyz, v_texcoord0);

		vec2 coords = LTC_Coords(dot(s.n, s.o), s.roughness);
		mat3 Minv   = identity33(); // identity matrix
		Lo_i   = LTC_Evaluate(s.n, s.o, s.p, Minv, u_quadPoints, u_twoSided, s_texFilteredMap);

		// scale by light intensity
		Lo_i *= u_lightIntensity;

		// scale by diffuse albedo
		Lo_i *= s.diffColor * u_albedo.rgb;

		// normalize
		Lo_i /= 2.0f * M_PI;

		if(u_displayMode == 1){
			float visibility = hardShadow(s_shadowMap,v_shadowcoord, shadowMapBias);
			Lo_i = Lo_i * visibility;
		}
	} else if(u_displayMode == 3) {
		float visibility = hardShadow(s_shadowMap,v_shadowcoord, shadowMapBias);
		Lo_i = vec3_splat(visibility);
	}else if(u_displayMode == 4) {	
		vec3 texCoord = v_shadowcoord.xyz / v_shadowcoord.w;

		float shadowMapDistance;
		if(any(greaterThan(texCoord.xy, vec2_splat(1.0))) || any(lessThan   (texCoord.xy, vec2_splat(0.0)))){
			shadowMapDistance = 1.0;	
		}else{
			shadowMapDistance = (unpackRgbaToFloat(texture2D(s_shadowMap, texCoord.xy)) - 0.4 )* 0.4;
		}
		Lo_i = vec3_splat(shadowMapDistance);
	}else if(u_displayMode == 5) {
		Lo_i = vec3_splat( ((v_shadowcoord.z - shadowMapBias) / v_shadowcoord.w -0.4)*0.4);
	}
	
    // set output
    gl_FragColor = vec4(Lo_i, 1.0);;
}
