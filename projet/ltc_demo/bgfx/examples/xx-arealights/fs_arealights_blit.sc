$input v_texcoord0

#include "common.sh"


vec3 rrt_odt_fit(vec3 v)
{
    vec3 a = v*(         v + 0.0245786) - 0.000090537;
    vec3 b = v*(0.983729*v + 0.4329510) + 0.238081;
    return a/b;
}

vec3 aces_fitted(vec3 color)
{
	mat3 ACES_INPUT_MAT = mat3_from_rows(
	    vec3( 0.59719, 0.35458, 0.04823),
	    vec3( 0.07600, 0.90834, 0.01566),
	    vec3( 0.02840, 0.13383, 0.83777));

	mat3 ACES_OUTPUT_MAT = mat3_from_rows(
	    vec3( 1.60475,-0.53108,-0.07367),
	    vec3(-0.10208, 1.10813,-0.00605),
	    vec3(-0.00327,-0.07276, 1.07602));

    color = mul(ACES_INPUT_MAT, color);

    // Apply RRT and ODT
    color = rrt_odt_fit(color);

    color = mul(ACES_OUTPUT_MAT, color);

    // Clamp to [0, 1]
    color = saturate(color);

    return color;
}


void main()
{
	if(u_displayMode != 2){
		vec4 samples = texture2D(s_texColorMap, v_texcoord0);

		// normalize
		float frameCount = u_sampleCount/float(NUM_SAMPLES) + 1.0;
		vec3 color = samples.rgb/frameCount;

		color = aces_fitted(color.rgb);
		color = toGammaAccurate(color);
		
		gl_FragColor = vec4(color, 1.0);

	}else{
		float depth = unpackRgbaToFloat(texture2D(s_texColorMap, v_texcoord0));
		gl_FragColor = vec4(depth,depth,depth, 1.0);
	}
}
