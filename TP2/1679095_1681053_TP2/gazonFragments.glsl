#version 430 core
struct Light
{
        vec3 Ambient; 
        vec3 Diffuse;
        vec3 Specular;
        vec4 Position;  // Si .w = 1.0 -> Direction de lumiere directionelle.
        vec3 SpotDir;
        float SpotExp;
        float SpotCutoff;
        vec3 Attenuation; //Constante, Lineraire, Quadratique
};

struct Mat
{
        vec4 Ambient; 
        vec4 Diffuse;
        vec4 Specular;
        vec4 Exponent;
        float Shininess;
};

uniform Light Lights[3]; // 0:ponctuelle  1:spot  2:directionnelle
uniform Mat Material;
uniform int pointLightOn;
uniform int spotLightOn;
uniform int dirLightOn;

uniform sampler2D colorMap;

in vec2 fragTexCoord;
in vec3 csPosition;
in vec3 normal_cameraSpace;

out vec4 color;


//Accumulateurs de contributions Ambiantes et Diffuses:
vec4 Ambient;
vec4 Diffuse;




// Calcul pour une lumi�re ponctuelle
void pointLight(in vec3 lightVect, in vec3 normal)
{
	float nDotVP;       // Produit scalaire entre VP et la normale
	float attenuation;  // facteur d'att�nuation calcul�
	float d;            // distance entre lumi�re et fragment
	vec3  VP;           // Vecteur lumi�re

						// Calculer vecteur lumi�re
	VP = lightVect;

	// Calculer distance � la lumi�re
	d = length(VP);

	// Normaliser VP
	VP = VP / d;

	// Calculer l'att�nuation due � la distance
	attenuation = 1.0 / (Lights[0].Attenuation[0] + Lights[0].Attenuation[1] * d + Lights[0].Attenuation[2] * d * d);


	nDotVP = max(dot(normal, VP), 0.0);

	// Calculer les contributions ambiantes et diffuses
	if (nDotVP > 0.0) {
		Ambient += vec4(Lights[0].Ambient, 1.0);
		Diffuse += vec4(Lights[0].Diffuse * nDotVP * attenuation, 1.0);
	}
}


// Calcul pour une lumi�re directionnelle
void directionalLight(in vec3 lightVect, in vec3 normal)
{
	vec3  VP;             // Vecteur lumi�re
	float nDotVP;         // Produit scalaire entre VP et la normale

	VP = normalize(-lightVect);
	nDotVP = max(dot(normal, VP), 0.0);

	// Calculer les contributions ambiantes et diffuses
	if (nDotVP > 0.0) {
		Ambient += vec4(Lights[2].Ambient, 1.0);
		Diffuse += vec4(Lights[2].Diffuse * nDotVP, 1.0);
	}
}


// Calcul pour une lumi�re "spot"
void spotLight(in vec3 lightVect, in vec3 normal)
{
	float nDotVP;             // Produit scalaire entre VP et la normale
	float spotAttenuation;    // Facteur d'att�nuation du spot
	float attenuation;        // Facteur d'att�nuation du � la distance
	float angleEntreLumEtSpot;// Angle entre le rayon lumieux et le milieu du cone
	float d;                  // Distance � la lumi�re
	vec3  VP;                 // Vecteur lumi�re

							  // Calculer vecteur lumi�re
	VP = lightVect;

	// Calculer distance � la lumi�re
	d = length(VP);

	// Normaliser VP
	VP = normalize(VP);

	// Calculer l'att�nuation due � la distance
	attenuation = 1.0 / (Lights[1].Attenuation[0] + Lights[1].Attenuation[1] * d + Lights[1].Attenuation[2] * d * d);

	// Le fragment est-il � l'int�rieur du c�ne de lumi�re ?
	vec3 spotDir = normalize(Lights[1].SpotDir);
	vec3 lightDir = normalize(-lightVect);
	angleEntreLumEtSpot = dot(spotDir, lightDir);
	float ambientAtt;

	if (acos(angleEntreLumEtSpot) > radians(Lights[1].SpotCutoff))
	{
		spotAttenuation = 0.0; // en dehors... aucune contribution
		ambientAtt = 0.0;
	}
	else
	{
		spotAttenuation = pow(angleEntreLumEtSpot, Lights[1].SpotExp);
		ambientAtt = 1.0;
	}

	// Combine les att�nuation du spot et de la distance
	attenuation *= spotAttenuation;

	nDotVP = dot(normal, VP);

	// Calculer les contributions ambiantes et diffuses
	if (nDotVP > 0.0) {
		Ambient += vec4(Lights[1].Ambient, 1.0) * ambientAtt;
		Diffuse += vec4(Lights[1].Diffuse * nDotVP * attenuation, 1.0);
	}
}

vec4 flight(in vec3 light0Vect, in vec3 light1Vect, in vec3 light2Vect, in vec3 normal)
{
	vec4 color;
	vec3 ecPosition3;

	// R�initialiser les accumulateurs
	Ambient = vec4(0.0);
	Diffuse = vec4(0.0);

	if (pointLightOn == 1) {
		pointLight(light0Vect, normal);
	}
	if (spotLightOn == 1) {
		spotLight(light1Vect, normal);
	}
	if (dirLightOn == 1) {
		directionalLight(light2Vect, normal);
	}

	color = (Ambient * Material.Ambient + Diffuse  * Material.Diffuse);
	color = clamp(color, 0.0, 1.0);

	return color;
}

void main (void) 
{
	vec3 light0Vect;
	vec3 light1Vect;
	vec3 light2Vect;
	vec4 out_color;
	vec4 textureColor;

	//Vecteurs de la surface vers la lumi�re
	light0Vect = Lights[0].Position.xyz - csPosition;
	light1Vect = Lights[1].Position.xyz - csPosition;
	light2Vect = Lights[2].Position.xyz;
	
	out_color = flight(light0Vect, light1Vect, light2Vect, normal_cameraSpace);
    
    //Contribution de la texture:
	textureColor = texture(colorMap, fragTexCoord);
	out_color *= textureColor;
    
    color = clamp(out_color, 0.0, 1.0);
    
    
    
   
}